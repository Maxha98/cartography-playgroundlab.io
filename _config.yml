# Site settings
title: "Cartography Playground"
description: "A simple and interactive website for explaining cartographic algorithms, problems and other matters."
theme_color: "#0065bd"
baseurl: "" # the subpath of your site
url: "https://cartography-playground.gitlab.io" # the base hostname & protocol for your site

# Link to source code repository
repository: "https://gitlab.com/cartography-playground/cartography-playground.gitlab.io"

# Dependencies
dep_materialdesignicons: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/3.5.95/css/materialdesignicons.min.css" integrity="sha256-gaCvS3Gc1xMFmZIK3NtGwbruVVajvayTTME6yrHanTA=" crossorigin="anonymous" />
dep_jquery: <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
dep_popperjs: <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha256-ZvOgfh+ptkpoa2Y4HkRY28ir89u/+VRyDE7sB7hEEcI=" crossorigin="anonymous"></script>
dep_bootstrapjs: <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha256-CjSoeELFOcH0/uxWu6mC/Vlrc1AARqbm/jiiImDGV3s=" crossorigin="anonymous"></script>
dep_mathjax: <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js" integrity="sha256-nvJJv9wWKEm88qvoQl9ekL2J+k/RWIsaSScxxlsrv8k=" crossorigin="anonymous"></script>
dep_sgvjs: <script src="https://cdnjs.cloudflare.com/ajax/libs/svg.js/2.7.1/svg.js" integrity="sha256-vSmnWtpaSGbnsDppQdGsUdfbXlGSn620VQ3dvXL4LcA=" crossorigin="anonymous"></script>
dep_jekyllsearch: <script src="https://cdnjs.cloudflare.com/ajax/libs/simple-jekyll-search/1.7.2/simple-jekyll-search.min.js" integrity="sha256-DsgE6Y6cI5eAZoTg8tJ5VBxFs+rnL8smPwt/u8tyAYo=" crossorigin="anonymous"></script>
dep_wordcloud: <script src="https://cdnjs.cloudflare.com/ajax/libs/wordcloud2.js/1.1.0/wordcloud2.min.js" integrity="sha256-mC52GeDp6AYGNYuDj0r1kWkCN4uL/SzMAw/F3WW+FHs=" crossorigin="anonymous"></script>
dep_chartjs: <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js" integrity="sha256-oSgtFCCmHWRPQ/JmR4OoZ3Xke1Pw4v50uh6pLcu+fIc=" crossorigin="anonymous"></script>
dep_spectrum: |
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css" integrity="sha256-f83N12sqX/GO43Y7vXNt9MjrHkPc4yi9Uq9cLy1wGIU=" crossorigin="anonymous" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js" integrity="sha256-ZdnRjhC/+YiBbXTHIuJdpf7u6Jh5D2wD5y0SNRWDREQ=" crossorigin="anonymous"></script>
dep_mapbox: |
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mapbox-gl@0.53.1/dist/mapbox-gl.css" integrity="sha256-98ZrstcQ9rIls5Ww035RRlxNlGm+oA27bvwgXd7+PHg=" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/mapbox-gl@0.53.1/dist/mapbox-gl.js" integrity="sha256-NupRsxv/ASVReWCqzKFyevqCV5Udonc+Yf0IuziWnyY=" crossorigin="anonymous"></script>
dep_mapboxgeocoder: |
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@mapbox/mapbox-gl-geocoder@3.1.6/dist/mapbox-gl-geocoder.css" integrity="sha256-0m2W2w/1AK8N1mtO4MeLbek6LBrJEUUNFC4LzUxybyI=" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/@mapbox/mapbox-gl-geocoder@3.1.6/dist/mapbox-gl-geocoder.min.js" integrity="sha256-PP0rHPmtgYRCx0iO0OH3ieqQbfo5By20375RXX6XP0o=" crossorigin="anonymous"></script>
dep_turfjs: <script src="https://cdnjs.cloudflare.com/ajax/libs/Turf.js/5.1.5/turf.min.js" integrity="sha256-V9GWip6STrPGZ47Fl52caWO8LhKidMGdFvZbjFUlRFs=" crossorigin="anonymous"></script>

# API Keys
key_googlemaps: "AIzaSyBFSgaZjabsmx9uBJi8_vSGCgRUc9Yilig"
key_bingmaps: "ArHeL25ojkxVSozLMyx_6C1wpiu72djIGjJxfYWbaeEuO-SUgSP4R2S56cgG9Vb5"
key_mapbox: "pk.eyJ1IjoiendlcmdlIiwiYSI6ImNqaXBwMGg0dDB6dDIzdnBtYzRmYXJnYW0ifQ.v3_u5cQDyFjv5j1wDe6oxg"

# Carto Playground Collection
collections:
  playgrounds:
    output: true

defaults:
-
  scope:
    path: ""
    type: playgrounds
  values:
    layout: playground
    permalink: /playgrounds/:title/

# Make all links pretty (no file extension)
permalink: pretty

# Build settings
future: true
markdown: kramdown
exclude: ["README.md", "CONTRIBUTING.md", "Gemfile", "Gemfile.lock"]

# compress html via compress liquid layout
compress_html:
  clippings: all
  comments: all
  ignore:
    envs: development

# compile sass in compressed mode
sass:
  style: compressed
  sass_dir: css/_scss

# compress js via plugin
# no config needed => see _plugins/js_compressor